
public class Mannschaft {

	private String name;
	private char spielklasse;

	public Mannschaft() {

	}

	public Mannschaft(String name, char spielklasse) {
		super();
		this.name = name;
		this.spielklasse = spielklasse;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(char spielklasse) {
		this.spielklasse = spielklasse;
	}

}
