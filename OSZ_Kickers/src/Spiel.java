
public class Spiel {

	private boolean heimspiel;
	private String datum;
	private int heimtore, gasttore, karteGelb, karteRot;

	public Spiel() {

	}

	public Spiel(boolean heimspiel, String datum, int heimtore, int gasttore) {
		this.heimspiel = heimspiel;
		this.datum = datum;
		this.heimtore = heimtore;
		this.gasttore = gasttore;
	}

	public int zaehleGelbeKarten(boolean hatGelbeKarte) {
		if (hatGelbeKarte == true)
			karteGelb = 1;
		else
			karteGelb = 0;
		return karteGelb;
	}

	public int zaehleRoteKarten(boolean hatRoteKarte) {
		if (hatRoteKarte == true)
			karteRot = 1;
		else
			karteRot = 0;
		return karteRot;
	}

	public boolean getHeimspiel() {
		return heimspiel;
	}

	public void setHeimspiel(boolean heimspiel) {
		this.heimspiel = heimspiel;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public int getHeimtore() {
		return heimtore;
	}

	public void setHeimtore(int heimtore) {
		this.heimtore = heimtore;
	}

	public int getGasttore() {
		return gasttore;
	}

	public void setGasttore(int gasttore) {
		this.gasttore = gasttore;
	}

}
