
public class Mannschaftsleiter extends Spieler {

	private String nameDerMannschaft;
	private double rabatt;

	public Mannschaftsleiter() {
	}

	public Mannschaftsleiter(String nameDerMannschaft, double rabatt) {
		super();
		this.nameDerMannschaft = nameDerMannschaft;
		this.rabatt = rabatt;
	}

	public String getNameDerMannschaft() {
		return nameDerMannschaft;
	}

	public void setNameDerMannschaft(String nameDerMannschaft) {
		this.nameDerMannschaft = nameDerMannschaft;
	}

	public double getRabatt() {
		return rabatt;
	}

	public void setRabatt(double rabatt) {
		this.rabatt = rabatt;
	}

}
