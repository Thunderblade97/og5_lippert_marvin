
public abstract class Personenkreis {

	private String vorname;
	private String nachname;
	private int telefonnummer;
	private boolean jahresbeitragGezahlt;

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public int getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isJahresbeitragGezahlt() {
		return jahresbeitragGezahlt;
	}

	public void setJahresbeitragGezahlt(boolean jahresbeitragGezahlt) {
		this.jahresbeitragGezahlt = jahresbeitragGezahlt;
	}

}
