
public class Trainer extends Personenkreis {

	private char lizenzklasse;
	private int aufwandentschaedigung;

	public Trainer() {
	}

	public Trainer(char lizenzklasse, int aufwandentschaedigung) {
		super();
		this.lizenzklasse = lizenzklasse;
		this.aufwandentschaedigung = aufwandentschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}

	public void setAufwandentschaedigung(int aufwandentschaedigung) {
		this.aufwandentschaedigung = aufwandentschaedigung;
	}
}
