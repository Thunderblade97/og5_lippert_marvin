
public class TestKickers {

	public static void main(String[] args) {
		Spieler Spieler = new Spieler();
		Spieler.setJahresbeitragGezahlt(true);
		Spieler.setVorname("J�rgen");
		Spieler.setNachname("Peters");
		Spieler.setTelefonnummer(1234567);
		Spieler.setSpielposition("Tor");
		Spieler.setTrikotnummer(1);

		Schiedsrichter Schiri = new Schiedsrichter();
		Schiri.setJahresbeitragGezahlt(false);
		Schiri.setVorname("Hans");
		Schiri.setNachname("M�ller");
		Schiri.setTelefonnummer(2345678);
		Schiri.setAnzahlGepfiffenenSpiele(103);

		Trainer Trainer = new Trainer();
		Trainer.setJahresbeitragGezahlt(false);
		Trainer.setVorname("Peter");
		Trainer.setNachname("Lustig");
		Trainer.setTelefonnummer(3456789);
		Trainer.setAufwandentschaedigung(350);
		Trainer.setLizenzklasse('B');

		Mannschaftsleiter Fuehrer = new Mannschaftsleiter();
		Fuehrer.setJahresbeitragGezahlt(true);
		Fuehrer.setVorname("Adolf");
		Fuehrer.setNachname("Rechts");
		Fuehrer.setTelefonnummer(47110815);
		Fuehrer.setNameDerMannschaft("Deutsche Einheit");
		Fuehrer.setRabatt(45);
		Fuehrer.setSpielposition("St�rmer");
		Fuehrer.setTrikotnummer(42);
	}

}
