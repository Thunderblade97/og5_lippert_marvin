
public class TestErweiterung {
	@SuppressWarnings("unused")
	public static void main(String[] args) {

		Mannschaft DeutscherVerein = new Mannschaft();
		Mannschaft DieAuslaenders = new Mannschaft();
			
		Spieler SpielerA = new Spieler();
		Spieler SpielerB = new Spieler();
		Spieler SpielerC = new Spieler();
		Spieler SpielerD = new Spieler();

		Schiedsrichter Schiri = new Schiedsrichter();
		Schiedsrichter Schiry = new Schiedsrichter();

		Schiri.setJahresbeitragGezahlt(false);
		Schiri.setVorname("Hans");
		Schiri.setNachname("M�ller");
		Schiri.setAnzahlGepfiffenenSpiele(103);		
		
		SpielerA.setJahresbeitragGezahlt(true);
		SpielerA.setVorname("J�rgen");
		SpielerA.setNachname("Peters");
		SpielerA.setSpielposition("Tor");
		SpielerA.setTrikotnummer(1);
		
		
		SpielerB.setJahresbeitragGezahlt(true);
		SpielerB.setVorname("Adolf");
		SpielerB.setNachname("Rechts");
		SpielerB.setSpielposition("St�rmer");
		SpielerB.setTrikotnummer(42);
		
		
		
		Schiry.setJahresbeitragGezahlt(false);
		Schiry.setVorname("Klaudia");
		Schiry.setNachname("Maier");
		Schiry.setAnzahlGepfiffenenSpiele(56);
		
		SpielerC.setJahresbeitragGezahlt(true);
		SpielerC.setVorname("Peter");
		SpielerC.setNachname("Meier");
		SpielerC.setSpielposition("Mittelfeld");
		SpielerC.setTrikotnummer(12);
		
		SpielerD.setJahresbeitragGezahlt(true);
		SpielerD.setVorname("Johannes");
		SpielerD.setNachname("Bauer");
		SpielerD.setSpielposition("Mittelfeld");
		SpielerD.setTrikotnummer(21);
	}
}
