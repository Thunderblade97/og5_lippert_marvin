
public class Spieler extends Personenkreis {

	private int trikotnummer;
	private String spielposition;
	private boolean hatGelbeKarte, hatRoteKarte;

	public Spieler() {
	}

	public Spieler(int trikotnummer, String spielposition, boolean hatGelbeKarte, boolean hatRoteKarte) {
		super();
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
		this.hatGelbeKarte = hatGelbeKarte;
		this.hatRoteKarte = hatRoteKarte;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public boolean isHatGelbeKarte() {
		return hatGelbeKarte;
	}

	public void setHatGelbeKarte(boolean hatGelbeKarte) {
		this.hatGelbeKarte = hatGelbeKarte;
	}

	public boolean isHatRoteKarte() {
		return hatRoteKarte;
	}

	public void setHatRoteKarte(boolean hatRoteKarte) {
		this.hatRoteKarte = hatRoteKarte;
	}
}
