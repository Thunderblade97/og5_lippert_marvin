
public class Schiedsrichter extends Personenkreis {

	private int anzahlGepfiffenenSpiele;

	public Schiedsrichter() {
	}

	public Schiedsrichter(int anzahlGepfiffenenSpiele) {
		super();
		this.anzahlGepfiffenenSpiele = anzahlGepfiffenenSpiele;
	}

	public int getAnzahlGepfiffenenSpiele() {
		return anzahlGepfiffenenSpiele;
	}

	public void setAnzahlGepfiffenenSpiele(int anzahlGepfiffenenSpiele) {
		this.anzahlGepfiffenenSpiele = anzahlGepfiffenenSpiele;
	}

}
