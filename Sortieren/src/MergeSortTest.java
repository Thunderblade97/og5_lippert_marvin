
public class MergeSortTest {
	public static void main(String[] args) {
		MergeSort ms = new MergeSort();

		long[] zahlenliste = { 5, 4, 8, 3, 9, 7, 6, 2, 1 };

		int l = 0, r = zahlenliste.length - 1;

		// Vor Sortierung
		System.out.println(MergeSort.array2str(zahlenliste));

		ms.mergesort(zahlenliste, l, r);
		// Nach Sortierung
		System.out.println(ms.mergesort(zahlenliste, l, r));
		System.out.println(ms.getDurchlauf());
	}
}
