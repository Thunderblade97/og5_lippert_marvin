public class BubbleSortTest {
	public static void main(String[] args) {
		BubbleSort bs = new BubbleSort();

		long[] zahlenliste = { 5, 4, 8, 3, 9, 7, 6, 2, 1 };
		// Vor Sortierung
		System.out.println(BubbleSort.array2str(zahlenliste));

		bs.sortiere(zahlenliste);
		// Nach Sortierung
		System.out.println(BubbleSort.array2str(zahlenliste));
		System.out.println(bs.getVertauschungen());
	}
}