
public class MergeSort {
	int q;
	int k;
	int durchlauf = 0;

	public String mergesort(long[] A, int l, int r) {
		do {
			int i = l;
			do {
				k = i;
				while ((i < r) && (A[i] <= A[i + 1])) {
					i++;
				}
				q = i;
				do {
					i++;
				} while ((i < r) && (A[i] <= A[i + 1]));
				if (i <= r) {
					merge(A, k, q, i);
					System.out.println(array2str(A));
					durchlauf++;

				}
			} while (i <= r);
		} while (k > l);
		return array2str(A);
	}

	public void merge(long[] A, int l, int q, int r) {
		q++;
		while ((l < q) && (q <= r)) {
			if (A[l] > A[q]) {
				long s = A[q];
				A[q] = A[l];
				A[l] = s;
				q++;
			}
			l++;
		}
	}

	public static String array2str(long[] A) {
		String result = "";

		for (int i = 0; i < A.length; i++) {

			result = result + A[i] + "  ";

		}
		return result;
	}

	public int getDurchlauf() {
		return durchlauf;
	}

	public void setDurchlauf(int durchlauf) {
		this.durchlauf = durchlauf;
	}
}
