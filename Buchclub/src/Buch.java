
public class Buch {

	private String autor, titel, isbn;

	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String toString() {
		return "[" + autor + ", " + titel + ", " + isbn + "]";
	}

	public boolean equals(Object o) {
		Buch b = (Buch) o;
		return this.isbn.equals(b.getIsbn());
	}

	public int compareTo(Object o) {
		Buch b = (Buch) o;
		if (this.isbn.compareTo(b.getIsbn()) < 0)
			return -1;
		else if (this.isbn.compareTo(b.getIsbn()) > 0)
			return 1;

		return 0;

	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

}
