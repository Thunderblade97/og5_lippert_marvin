package zahlensortierer;

import java.util.*;

//TODO add your name as author
/**
 * @author Marvin Lippert Version 1
 */
public class Zahlensortierer {

	// TODO
	// vervollst�ndigen Sie die main methode so, dass sie 3 Zahlen vom Benutzer
	// einliest und die kleinste und gr��te Zahl ausgibt.
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.print("1. Zahl: ");
		int zahl1 = scan.nextInt();

		System.out.print("2. Zahl: ");
		int zahl2 = scan.nextInt();

		System.out.print("3. Zahl: ");
		int zahl3 = scan.nextInt();

		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Die Zahl " + zahl1 + " ist die gr��te.");
		}
		if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println("Die Zahl " + zahl2 + " ist die gr��te.");
		}
		if (zahl3 > zahl1 && zahl3 > zahl2) {
			System.out.println("Die Zahl " + zahl3 + " ist die gr��te.");
		}
		if (zahl1 < zahl2 && zahl1 < zahl3) {
			System.out.println("Die Zahl " + zahl1 + " ist die kleinste.");
		}
		if (zahl2 < zahl1 && zahl2 < zahl3) {
			System.out.println("Die Zahl " + zahl2 + " ist die kleinste.");
		}
		if (zahl3 < zahl1 && zahl3 < zahl2) {
			System.out.println("Die Zahl " + zahl3 + " ist die kleinste.");
		}

		scan.close();
	}
}
